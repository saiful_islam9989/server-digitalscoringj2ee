package com.demo.j2ee;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.opencv.core.Core;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

//import sun.org.mozilla.javascript.internal.json.JsonParser;

public class ImageProcessor {

	// private static Bitmap bmp, drawnImg, bmpimg1, bmpimg2;
	private static Mat targetImage, templateImage, descriptors, dupDescriptors;
	private static FeatureDetector detector;
	private static DescriptorExtractor DescExtractor;
	private static DescriptorMatcher matcher;
	private static MatOfKeyPoint keypoints, dupKeypoints;
	private static MatOfDMatch matches, matches_final_mat;

	// private static boolean isDuplicate = false;
	// private static Scalar RED = new Scalar(255, 0, 0);
	// private static Scalar GREEN = new Scalar(0, 255, 0);
	// //private static int descriptor = DescriptorExtractor.BRISK;
	private static int min_dist = 80; // 10,750;;; 80,10
	private static int min_matches = 100;
	static int result;
	static int matchesFound;
	private static String finalMatchedValue = "";
	public static String totalResult = "";

	public static Mat matchingResult;
	static int topofTargetImg;
	static int bottomofTargetImg;
	static int leftofTargetImg;
	static int rightofTargetImg;

	//public static final String  BASE_DIR = "/home/uapthesis_hw/uploadedImage/";
	public static final String  BASE_DIR = "/home/saiful/Documents/J2ee project/data";
	//public static final String  BASE_DIR = "E:\\upload\\";

	static List<org.opencv.features2d.DMatch> finalMatchesList;

	public static String getResult(String targetFile, String templateFile) {

		// System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
		// System.out.println(System.getProperty("java.library.path"));

		try {
			finalMatchedValue = withThresholding(targetFile, templateFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// finalMatchedValue=withoutThresholding(targetFile, templateFile);

		return finalMatchedValue;
	}

	private static String withoutThresholding(String targetFile, String templateFile) {
		// TODO Auto-generated method stub
		matchingResult = new Mat();

		Mat img = Highgui.imread(targetFile);
		Mat templ = Highgui.imread(templateFile);

		Imgproc.cvtColor(img, img, Imgproc.COLOR_RGBA2GRAY);
		Imgproc.cvtColor(templ, templ, Imgproc.COLOR_RGBA2GRAY);

		// System.out.println(img + " " + templ + "");

		img.convertTo(img, CvType.CV_32F);
		templ.convertTo(templ, CvType.CV_32F);

		Mat hist1 = new Mat();
		Mat hist2 = new Mat();

		MatOfInt histSize = new MatOfInt(180);
		MatOfInt channels = new MatOfInt(0);

		ArrayList<Mat> bgr_planes1 = new ArrayList<Mat>();
		ArrayList<Mat> bgr_planes2 = new ArrayList<Mat>();

		Core.split(img, bgr_planes1);
		Core.split(templ, bgr_planes2);//

		MatOfFloat histRanges = new MatOfFloat(0f, 180f);
		boolean accumulate = false;

		Imgproc.calcHist(bgr_planes1, channels, new Mat(), hist1, histSize, histRanges, accumulate);
		Core.normalize(hist1, hist1, 0, hist1.rows(), Core.NORM_MINMAX, -1, new Mat());

		Imgproc.calcHist(bgr_planes2, channels, new Mat(), hist2, histSize, histRanges, accumulate);
		Core.normalize(hist2, hist2, 0, hist2.rows(), Core.NORM_MINMAX, -1, new Mat());

		hist1.convertTo(hist1, CvType.CV_32F);
		hist2.convertTo(hist2, CvType.CV_32F);

		double compare = Imgproc.compareHist(hist1, hist2, Imgproc.CV_COMP_CORREL);
		//System.out.println("compare Value" + compare);
		return compare + "";

	}

	private static String withThresholding (String targetFile, String templateFile) throws IOException {
		// TODO Auto-generated method stub
		double compare = 0.0;
		matchingResult = new Mat();
		Mat img; // =Highgui.imread(targetFile);
		Mat templ;// = Highgui.imread(templateFile);

		targetImage = Highgui.imread(targetFile);
		templateImage = Highgui.imread(templateFile);

		// converting threshold

		
		  /*BufferedImage bufferedImage;
	         
	         
	       //read image file
	   	  bufferedImage = ImageIO.read(new File(targetFile));

	   	  // create a blank, RGB, same width and height, and a white background
	   	  BufferedImage newBufferedImage = new BufferedImage(bufferedImage.getWidth(),
	   			bufferedImage.getHeight(), BufferedImage.TYPE_BYTE_BINARY);
	   	  newBufferedImage.createGraphics().drawImage(bufferedImage, 0, 0, Color.WHITE, null);
	   	  
	   	targetImage = new Mat(newBufferedImage.getHeight(), newBufferedImage.getWidth(), CvType.CV_8UC3);
	    byte[] imageData = ((DataBufferByte) newBufferedImage.getRaster().getDataBuffer()).getData();
	    targetImage.put(0, 0, imageData);
*/	    
		
		
		
		
		
		
		//BufferedImage image = ImageIO.read(new File("e://upload/newphotos/nwhite.jpg"));
		
		/*BufferedImage image = ImageIO.read(new File(BASE_DIR+"white_bg.jpg"));
		

	     BufferedImage overlay = ImageIO.read(new File(BASE_DIR, "target.png"));

	     // BufferedImage image=src.getBufferedImage();
	   // BufferedImage overlay =tmp.getBufferedImage();
	    // create the new image, canvas size is the max. of both image sizes
	    int w = Math.max(image.getWidth(), overlay.getWidth());
	    int h = Math.max(image.getHeight(), overlay.getHeight());
	      //int w=tmp.width();
	     // int h=tmp.height();
	    BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

	    // paint both images, preserving the alpha channels
	    Graphics g = combined.getGraphics();
	    g.drawImage(image, 0, 0, null);
	    g.drawImage(overlay, 41, 30, null);

	    // Save as new image

	       ImageIO.write(combined, "PNG", new File(BASE_DIR, "combined.png"));
	       targetFile = BASE_DIR+"combined.png";
*/		
		img = new Mat();
		templ = new Mat();

		targetImage = Highgui.imread(targetFile, Highgui.CV_LOAD_IMAGE_COLOR);
		img = new Mat(targetImage.rows(), targetImage.cols(), targetImage.type());
		img = targetImage;
		Imgproc.threshold(targetImage, img, 127, 255, Imgproc.THRESH_BINARY_INV);
		//Highgui.imwrite("e://upload/final pic/targetthres.png", img);

		// Highgui.imwrite("sample.png", destination);

		templateImage = Highgui.imread(templateFile, Highgui.CV_LOAD_IMAGE_COLOR);
		templ = new Mat(templateImage.rows(), templateImage.cols(), templateImage.type());
		templ = templateImage;
		Imgproc.threshold(templateImage, templ, 127, 255, Imgproc.THRESH_BINARY_INV);
		//Highgui.imwrite("e://upload/final pic/tempthres.png", img);
// ------------threshold complete-------------//

		
		
		/* scaleing works are done below*/
		
		Mat TargetImageScaleing = targetImage.submat(getStartRow(targetImage), getEndRow(targetImage),
				getStartCol(targetImage), getEndCol(targetImage));
		//Highgui.imwrite("e://upload/final pic/targetImage_scl.png", TargetImageScaleing);
		
		Mat TemplateImageScaleing= templateImage.submat(getStartRow(templateImage), getEndRow(templateImage),
				getStartCol(templateImage), getEndCol(templateImage));
		//Highgui.imwrite("e://upload/final pic/templateImage_scl.png", TemplateImageScaleing);
		
		/* scaling works complete here*/
		
		/* Resize */
		
		Mat ResizedTargetImg = new Mat();
		Size SizeofResizedTargetImg = new Size(100,100);
		Imgproc.resize(TargetImageScaleing, ResizedTargetImg, SizeofResizedTargetImg);
		//Highgui.imwrite("e://upload/final pic/resizeTargetImage.png", ResizedTargetImg);
		
		Mat ResizedTemplateImg = new Mat();
		Size SizeofResizedTemplateImg = new Size(100,100);
		Imgproc.resize(TemplateImageScaleing, ResizedTemplateImg, SizeofResizedTemplateImg);
		//Highgui.imwrite("e://upload/final pic/resizeTemplateImage.png", ResizedTemplateImg);
		
		/*resize complete*/
		
		
		/*Add border in target image start*/
		
		topofTargetImg = (int)(0.10*ResizedTargetImg.rows());
		bottomofTargetImg = (int)(0.10*ResizedTargetImg.rows());
		rightofTargetImg = (int)(0.10*ResizedTargetImg.cols());
		leftofTargetImg = (int)(0.10*ResizedTargetImg.cols());
		
		Mat largeTargetImg;// = new Mat(TargetImageScaleing.rows(),TargetImageScaleing.cols(),TargetImageScaleing.type());
		largeTargetImg = ResizedTargetImg;
		
		Imgproc.copyMakeBorder(ResizedTargetImg, largeTargetImg, topofTargetImg, bottomofTargetImg, leftofTargetImg, rightofTargetImg, Imgproc.BORDER_CONSTANT);
	    //Highgui.imwrite("e://upload/final pic/LargeTargetimgpad.png", largeTargetImg);
		
		/*Add border on target image finish*/
		
	
		int result_cols = largeTargetImg.cols() - ResizedTemplateImg.cols() + 1;
		int result_rows = largeTargetImg.rows() - ResizedTemplateImg.rows() + 1;
		matchingResult.create(result_rows, result_cols, CvType.CV_32FC1);

		Imgproc.matchTemplate(largeTargetImg, ResizedTemplateImg, matchingResult, Imgproc.TM_CCOEFF_NORMED);

		/// Localizing the best match with minMaxLoc
		double minVal;
		double maxVal;
		Point minLoc;
		Point maxLoc;
		Point matchLoc;

		// / Localizing the best match with minMaxLoc
		MinMaxLocResult mmr;
		mmr = Core.minMaxLoc(matchingResult);
		minVal = mmr.minVal;
		maxVal = mmr.maxVal;
		//System.out.println("before converting matrix value in absolute format");
		//System.out.println("maxvalue " + maxVal + " minval " + minVal);

		// after abssoluting

		// double picdata[][] = new double[result_rows][result_cols] ;

		double temp[];
		for (int a = 0; a < result_rows; a++) {
			for (int b = 0; b < result_cols; b++) {
				temp = matchingResult.get(a, b);
				double data = Math.abs(temp[0]);
				// picdata[a][b]=temp[0];
				matchingResult.put(a, b, data);
				// String s = a+" "+b+" ";
				// System.out.print(picdata[a][b]+" ");
				// System.out.println(matchingResult.dump());

			}
		}
		// System.out.println(matchingResult.dump());

		// / Localizing the best match with minMaxLoc
		mmr = Core.minMaxLoc(matchingResult);
		minVal = mmr.minVal;
		maxVal = mmr.maxVal * 10;
		int value = (int) Math.ceil(maxVal);
		
		System.out.println();
		System.out.println("after converting matrix value in absolute format");
		System.out.println("maxvalue " + maxVal + " minval " + minVal);

		// System.out.println(matchingResult.dump());

		
		
		
		return "Your Score: " + value ;

	}

	private static int getEndRow(Mat img) {

		boolean FLAG_ROW = false;
		int end_row = 0;
		for (int row = img.rows() - 1; row >= 0; row--) {
			for (int col = img.cols() - 1; col >= 0; col--) {
				double getRes[];
				getRes = img.get(row, col);
				if (getRes[0] == 255) {
					FLAG_ROW = true;
					break;

				}
				end_row = row;

			}
			if (FLAG_ROW) {
				break;
			}
		}

		return end_row;

	}

	private static int getEndCol(Mat img) {
		boolean FLAG_COL = false;
		int end_col = 0;
		for (int col = img.cols() - 1; col >= 0; col--) {
			for (int row = img.rows() - 1; row >= 0; row--) {
				double getRes[];
				getRes = img.get(row, col);
				if (getRes[0] == 255) {
					FLAG_COL = true;
					break;

				}
				end_col = col;

			}
			if (FLAG_COL) {
				break;
			}
		}

		return end_col;
	}

	private static int getStartCol(Mat img) {
		boolean FLAG_COL = false;
		int end_col = 0;
		for (int col = 0; col < img.cols(); col++) {
			for (int row = 0; row < img.rows(); row++) {
				double getRes[];
				getRes = img.get(row, col);
				if (getRes[0] == 255) {
					FLAG_COL = true;
					break;

				}
				end_col = col;

			}
			if (FLAG_COL) {
				break;
			}
		}

		return end_col;
	}

	private static int getStartRow(Mat img) {
		boolean FLAG_ROW = false;
		int end_row = 0;
		for (int row = 0; row < img.rows(); row++) {
			for (int col = 0; col < img.cols(); col++) {
				double getRes[];
				getRes = img.get(row, col);
				if (getRes[0] == 255) {
					FLAG_ROW = true;
					break;

				}
				end_row = row;
			}
			if (FLAG_ROW) {
				break;
			}
		}

		return end_row;

	}

	private static int compare(Mat img, Mat templ, int descriptor) {
		// TODO Auto-generated method stub

		try {

			Imgproc.cvtColor(targetImage, targetImage, Imgproc.COLOR_BGR2RGB);
			Imgproc.cvtColor(templateImage, templateImage, Imgproc.COLOR_BGR2RGB);

			detector = FeatureDetector.create(FeatureDetector.PYRAMID_FAST);
			DescExtractor = DescriptorExtractor.create(descriptor);

			matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING);

			keypoints = new MatOfKeyPoint();
			dupKeypoints = new MatOfKeyPoint();
			descriptors = new Mat();
			dupDescriptors = new Mat();
			matches = new MatOfDMatch();

			detector.detect(img, keypoints);

			detector.detect(templ, dupKeypoints);

			// Descript keypoints
			DescExtractor.compute(img, keypoints, descriptors);

			DescExtractor.compute(templ, dupKeypoints, dupDescriptors);

			// matching descriptors
			matcher.match(descriptors, dupDescriptors, matches);

			// New method of finding best matches
			List<org.opencv.features2d.DMatch> matchesList = matches.toList();
			List<org.opencv.features2d.DMatch> matches_final = new ArrayList<org.opencv.features2d.DMatch>();

			for (int i = 0; i < matchesList.size(); i++) {
				if (matchesList.get(i).distance <= min_dist) {
					matches_final.add(matches.toList().get(i));
				}
			}

			matches_final_mat = new MatOfDMatch();
			matches_final_mat.fromList(matches_final);

			finalMatchesList = matches_final_mat.toList();

			matchesFound = finalMatchesList.size();

			/*
			 * Features2d.drawMatches(img1, keypoints, img2, dupKeypoints,
			 * matches_final_mat, img3, GREEN, RED, drawnMatches,
			 * Features2d.NOT_DRAW_SINGLE_POINTS);
			 */

			// System.out.println(keypoints+"/n"+dupKeypoints+"/n"+matches_final_mat+"/n"+matchesFound);
			System.out.println("Keypoints: " + keypoints);

			//
		} catch (Exception e) {
			e.printStackTrace();
		}
		// return finalMatchesList;
		return matchesFound;
	}
}