package com.demo.j2ee;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.opencv.core.Core;

/**
 * @author al noman
 * 
 * @desc Servlet implementation class UploadController
 */
public class UploadController extends HttpServlet {
	static {
		//System.load(new File("C:\\OpenCV\\opencv\\build\\java\\x64\\opencv_java249.dll").getAbsolutePath());
		//System.load(new File("/home/uapthesis_hw/opencv-2.4.9/build/lib/libopencv_java249.so").getAbsolutePath());
		System.load(new File("/home/saiful/Downloads/Softwares/opencv-2.4.9/build/lib/libopencv_java249.so").getAbsolutePath());
	}

	int i = 1;
	File path;
	int marker;
	private static final long serialVersionUID = 1L;
	String uploadedImageURL = "target";
	private String folderLocation = null;

	@Override
	public void init() throws ServletException {
		super.init();
		this.folderLocation = getServletContext().getInitParameter("UPLOAD_FOLDER");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		try {
			HashMap<String, String> formParams = new HashMap<String, String>();

			// Check that we have a file upload request
			boolean isMultipart = ServletFileUpload.isMultipartContent(request);

			if (isMultipart) {

				// Create a factory for disk-based file items
				FileItemFactory factory = new DiskFileItemFactory();

				// Create a new file upload handler
				ServletFileUpload upload = new ServletFileUpload(factory);

				// Parse the request
				List<FileItem> items = upload.parseRequest(request);

				// Process the uploaded items
				Iterator<FileItem> iter = items.iterator();
				while (iter.hasNext()) {
					FileItem item = (FileItem) iter.next();
					if (item.isFormField()) {

						// Process a regular form field
						formParams.put(item.getFieldName(), item.getString());
					} else {

						// Process a file upload
						String fileName = item.getName();

						path = new File(ImageProcessor.BASE_DIR);
						//
						if (!path.exists()) {
							boolean status = path.mkdirs();
						}
						File uploadedFile = new File(path + "/" + uploadedImageURL + ".png");
	
						item.write(uploadedFile);
						System.out.println(System.getProperty("java.library.path"));
						uploadedImageURL = "template";
						
					}

				} //end of while loop
			} //end of if scope
			
		
			String data = ImageProcessor.getResult (path + "/" + "target.png", path + "/" + "template.png");

			System.out.println("data "+data);
			 response.getWriter().println(data);
			
		} catch (Exception e) {
			uploadedImageURL = "target";
			 response.getWriter().println(e.getMessage());
			throw new ServletException(e.getMessage());
		}

		//1st hate likha , 2nd ovro
		uploadedImageURL = "target";
					
	}

}




/*	response.getWriter().println(ImageProcessor.getResult(path + "/" + "34ta.png", path + "/" + "34to.PNG"));
System.out.println();

response.getWriter().println(ImageProcessor.getResult(path + "/" + "35tha (2).png", path + "/" + "35tha.PNG"));
System.out.println();

response.getWriter().println(ImageProcessor.getResult(path + "/" + "36dontanno.png", path + "/" + "36no.PNG"));
System.out.println();

response.getWriter().println(ImageProcessor.getResult(path + "/" + "37mudhanno.png", path + "/" + "37muddhanno.PNG"));
System.out.println();

response.getWriter().println(ImageProcessor.getResult(path + "/" + "38doshunnoro.png", path + "/" + "38dosunnoro.PNG"));
System.out.println();

response.getWriter().println(ImageProcessor.getResult(path + "/" + "39dhoshunnoro.png", path + "/" + "39dhosunnoro.PNG"));
System.out.println();

response.getWriter().println(ImageProcessor.getResult(path + "/" + "40lo (2).png", path + "/" + "40lo.PNG"));
System.out.println();

response.getWriter().println(ImageProcessor.getResult(path + "/" + "41dontosso (2).png", path + "/" + "41dontosso.PNG"));
System.out.println();

response.getWriter().println(ImageProcessor.getResult(path + "/" + "42ho (2).png", path + "/" + "42ho.PNG"));
System.out.println();

response.getWriter().println(ImageProcessor.getResult(path + "/" + "43muddhanosso (2).png", path + "/" + "43muddhanosso.PNG"));
System.out.println();

response.getWriter().println(ImageProcessor.getResult(path + "/" + "44ro (2).png", path + "/" + "44ro.PNG"));
System.out.println();

response.getWriter().println(ImageProcessor.getResult(path + "/" + "45talibosso (2).png", path + "/" + "45talibosso.PNG"));
System.out.println();

response.getWriter().println(ImageProcessor.getResult(path + "/" + "46untojjoyo.png", path + "/" + "46ontojjoyo.PNG"));
System.out.println();

response.getWriter().println(ImageProcessor.getResult(path + "/" + "49khondotto (2).png", path + "/" + "49khondotto.PNG"));
*/
;
